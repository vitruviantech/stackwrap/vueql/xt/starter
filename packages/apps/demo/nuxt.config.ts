// Define unique development and Vite HMR server ports per each app.
// NOTE: Don't forget to expose these ports in `docker-compose.yml`.
const NUXT_DEV_SERVER_PORT=3001
const NUXT_VITE_HMR_PORT=24679


// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },

  // @ts-ignore
  extends: [
    // @ts-ignore
    process.env.VUEQL_LAYERS.split(',')
  ],

  // Modules:
  // * Use package name for 3rd-party library usage, or,
  // * Use file path for live module development (hot reloading, etc.)
  modules: [
    '@stackwrap/vueql-example-module'
  ],

  devServer: {
    port: NUXT_DEV_SERVER_PORT
  },

  vite: {
    server: {
      hmr: {
        clientPort: NUXT_VITE_HMR_PORT,
        port: NUXT_VITE_HMR_PORT
      }
    }
  },

  vueql: {
    server: {
      graphql: {
        schema: ['https://rickandmortyapi.com/graphql']
      }
    }
  }
})
