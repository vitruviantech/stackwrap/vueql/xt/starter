import { GraphQLResolveInfo, GraphQLScalarType, GraphQLScalarTypeConfig } from 'graphql';
import { offlineExchange } from '@urql/exchange-graphcache';
import { Resolver as GraphCacheResolver, UpdateResolver as GraphCacheUpdateResolver, OptimisticMutationResolver as GraphCacheOptimisticMutationResolver } from '@urql/exchange-graphcache';

import { gql } from '@urql/vue';
import * as Urql from '@urql/vue';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = { [_ in K]?: never };
export type Incremental<T> = T | { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never };
export type RequireFields<T, K extends keyof T> = Omit<T, K> & { [P in K]-?: NonNullable<T[P]> };
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string; }
  String: { input: string; output: string; }
  Boolean: { input: boolean; output: boolean; }
  Int: { input: number; output: number; }
  Float: { input: number; output: number; }
  /** The `Upload` scalar type represents a file upload. */
  Upload: { input: any; output: any; }
};

export enum CacheControlScope {
  Private = 'PRIVATE',
  Public = 'PUBLIC'
}

export type Character = {
  __typename?: 'Character';
  /** Time at which the character was created in the database. */
  created?: Maybe<Scalars['String']['output']>;
  /** Episodes in which this character appeared. */
  episode: Array<Maybe<Episode>>;
  /** The gender of the character ('Female', 'Male', 'Genderless' or 'unknown'). */
  gender?: Maybe<Scalars['String']['output']>;
  /** The id of the character. */
  id?: Maybe<Scalars['ID']['output']>;
  /**
   * Link to the character's image.
   * All images are 300x300px and most are medium shots or portraits since they are intended to be used as avatars.
   */
  image?: Maybe<Scalars['String']['output']>;
  /** The character's last known location */
  location?: Maybe<Location>;
  /** The name of the character. */
  name?: Maybe<Scalars['String']['output']>;
  /** The character's origin location */
  origin?: Maybe<Location>;
  /** The species of the character. */
  species?: Maybe<Scalars['String']['output']>;
  /** The status of the character ('Alive', 'Dead' or 'unknown'). */
  status?: Maybe<Scalars['String']['output']>;
  /** The type or subspecies of the character. */
  type?: Maybe<Scalars['String']['output']>;
};

export type Characters = {
  __typename?: 'Characters';
  info?: Maybe<Info>;
  results?: Maybe<Array<Maybe<Character>>>;
};

export type Continent = {
  __typename?: 'Continent';
  code: Scalars['ID']['output'];
  countries: Array<Country>;
  name: Scalars['String']['output'];
};

export type ContinentFilterInput = {
  code?: InputMaybe<StringQueryOperatorInput>;
};

export type Country = {
  __typename?: 'Country';
  awsRegion: Scalars['String']['output'];
  capital?: Maybe<Scalars['String']['output']>;
  code: Scalars['ID']['output'];
  continent: Continent;
  currencies: Array<Scalars['String']['output']>;
  currency?: Maybe<Scalars['String']['output']>;
  emoji: Scalars['String']['output'];
  emojiU: Scalars['String']['output'];
  languages: Array<Language>;
  name: Scalars['String']['output'];
  native: Scalars['String']['output'];
  phone: Scalars['String']['output'];
  phones: Array<Scalars['String']['output']>;
  states: Array<State>;
  subdivisions: Array<Subdivision>;
};


export type CountryNameArgs = {
  lang?: InputMaybe<Scalars['String']['input']>;
};

export type CountryFilterInput = {
  code?: InputMaybe<StringQueryOperatorInput>;
  continent?: InputMaybe<StringQueryOperatorInput>;
  currency?: InputMaybe<StringQueryOperatorInput>;
};

export type Episode = {
  __typename?: 'Episode';
  /** The air date of the episode. */
  air_date?: Maybe<Scalars['String']['output']>;
  /** List of characters who have been seen in the episode. */
  characters: Array<Maybe<Character>>;
  /** Time at which the episode was created in the database. */
  created?: Maybe<Scalars['String']['output']>;
  /** The code of the episode. */
  episode?: Maybe<Scalars['String']['output']>;
  /** The id of the episode. */
  id?: Maybe<Scalars['ID']['output']>;
  /** The name of the episode. */
  name?: Maybe<Scalars['String']['output']>;
};

export type Episodes = {
  __typename?: 'Episodes';
  info?: Maybe<Info>;
  results?: Maybe<Array<Maybe<Episode>>>;
};

export type FilterCharacter = {
  gender?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  species?: InputMaybe<Scalars['String']['input']>;
  status?: InputMaybe<Scalars['String']['input']>;
  type?: InputMaybe<Scalars['String']['input']>;
};

export type FilterEpisode = {
  episode?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
};

export type FilterLocation = {
  dimension?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  type?: InputMaybe<Scalars['String']['input']>;
};

export type Info = {
  __typename?: 'Info';
  /** The length of the response. */
  count?: Maybe<Scalars['Int']['output']>;
  /** Number of the next page (if it exists) */
  next?: Maybe<Scalars['Int']['output']>;
  /** The amount of pages. */
  pages?: Maybe<Scalars['Int']['output']>;
  /** Number of the previous page (if it exists) */
  prev?: Maybe<Scalars['Int']['output']>;
};

export type Language = {
  __typename?: 'Language';
  code: Scalars['ID']['output'];
  name: Scalars['String']['output'];
  native: Scalars['String']['output'];
  rtl: Scalars['Boolean']['output'];
};

export type LanguageFilterInput = {
  code?: InputMaybe<StringQueryOperatorInput>;
};

export type Location = {
  __typename?: 'Location';
  /** Time at which the location was created in the database. */
  created?: Maybe<Scalars['String']['output']>;
  /** The dimension in which the location is located. */
  dimension?: Maybe<Scalars['String']['output']>;
  /** The id of the location. */
  id?: Maybe<Scalars['ID']['output']>;
  /** The name of the location. */
  name?: Maybe<Scalars['String']['output']>;
  /** List of characters who have been last seen in the location. */
  residents: Array<Maybe<Character>>;
  /** The type of the location. */
  type?: Maybe<Scalars['String']['output']>;
};

export type Locations = {
  __typename?: 'Locations';
  info?: Maybe<Info>;
  results?: Maybe<Array<Maybe<Location>>>;
};

export type Query = {
  __typename?: 'Query';
  /** Get a specific character by ID */
  character?: Maybe<Character>;
  /** Get the list of all characters */
  characters?: Maybe<Characters>;
  /** Get a list of characters selected by ids */
  charactersByIds?: Maybe<Array<Maybe<Character>>>;
  continent?: Maybe<Continent>;
  continents: Array<Continent>;
  countries: Array<Country>;
  country?: Maybe<Country>;
  /** Get a specific episode by ID */
  episode?: Maybe<Episode>;
  /** Get the list of all episodes */
  episodes?: Maybe<Episodes>;
  /** Get a list of episodes selected by ids */
  episodesByIds?: Maybe<Array<Maybe<Episode>>>;
  language?: Maybe<Language>;
  languages: Array<Language>;
  /** Get a specific locations by ID */
  location?: Maybe<Location>;
  /** Get the list of all locations */
  locations?: Maybe<Locations>;
  /** Get a list of locations selected by ids */
  locationsByIds?: Maybe<Array<Maybe<Location>>>;
};


export type QueryCharacterArgs = {
  id: Scalars['ID']['input'];
};


export type QueryCharactersArgs = {
  filter?: InputMaybe<FilterCharacter>;
  page?: InputMaybe<Scalars['Int']['input']>;
};


export type QueryCharactersByIdsArgs = {
  ids: Array<Scalars['ID']['input']>;
};


export type QueryContinentArgs = {
  code: Scalars['ID']['input'];
};


export type QueryContinentsArgs = {
  filter?: InputMaybe<ContinentFilterInput>;
};


export type QueryCountriesArgs = {
  filter?: InputMaybe<CountryFilterInput>;
};


export type QueryCountryArgs = {
  code: Scalars['ID']['input'];
};


export type QueryEpisodeArgs = {
  id: Scalars['ID']['input'];
};


export type QueryEpisodesArgs = {
  filter?: InputMaybe<FilterEpisode>;
  page?: InputMaybe<Scalars['Int']['input']>;
};


export type QueryEpisodesByIdsArgs = {
  ids: Array<Scalars['ID']['input']>;
};


export type QueryLanguageArgs = {
  code: Scalars['ID']['input'];
};


export type QueryLanguagesArgs = {
  filter?: InputMaybe<LanguageFilterInput>;
};


export type QueryLocationArgs = {
  id: Scalars['ID']['input'];
};


export type QueryLocationsArgs = {
  filter?: InputMaybe<FilterLocation>;
  page?: InputMaybe<Scalars['Int']['input']>;
};


export type QueryLocationsByIdsArgs = {
  ids: Array<Scalars['ID']['input']>;
};

export type State = {
  __typename?: 'State';
  code?: Maybe<Scalars['String']['output']>;
  country: Country;
  name: Scalars['String']['output'];
};

export type StringQueryOperatorInput = {
  eq?: InputMaybe<Scalars['String']['input']>;
  in?: InputMaybe<Array<Scalars['String']['input']>>;
  ne?: InputMaybe<Scalars['String']['input']>;
  nin?: InputMaybe<Array<Scalars['String']['input']>>;
  regex?: InputMaybe<Scalars['String']['input']>;
};

export type Subdivision = {
  __typename?: 'Subdivision';
  code: Scalars['ID']['output'];
  emoji?: Maybe<Scalars['String']['output']>;
  name: Scalars['String']['output'];
};



export type ResolverTypeWrapper<T> = Promise<T> | T;


export type ResolverWithResolve<TResult, TParent, TContext, TArgs> = {
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};
export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> = ResolverFn<TResult, TParent, TContext, TArgs> | ResolverWithResolve<TResult, TParent, TContext, TArgs>;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterable<TResult> | Promise<AsyncIterable<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<TResult, TKey extends string, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<{ [key in TKey]: TResult }, TParent, TContext, TArgs>;
  resolve?: SubscriptionResolveFn<TResult, { [key in TKey]: TResult }, TContext, TArgs>;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<TResult, TKey extends string, TParent, TContext, TArgs> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<TResult, TKey extends string, TParent = {}, TContext = {}, TArgs = {}> =
  | ((...args: any[]) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes> | Promise<Maybe<TTypes>>;

export type IsTypeOfResolverFn<T = {}, TContext = {}> = (obj: T, context: TContext, info: GraphQLResolveInfo) => boolean | Promise<boolean>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;



/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = {
  Boolean: ResolverTypeWrapper<Scalars['Boolean']['output']>;
  CacheControlScope: CacheControlScope;
  Character: ResolverTypeWrapper<Character>;
  Characters: ResolverTypeWrapper<Characters>;
  Continent: ResolverTypeWrapper<Continent>;
  ContinentFilterInput: ContinentFilterInput;
  Country: ResolverTypeWrapper<Country>;
  CountryFilterInput: CountryFilterInput;
  Episode: ResolverTypeWrapper<Episode>;
  Episodes: ResolverTypeWrapper<Episodes>;
  FilterCharacter: FilterCharacter;
  FilterEpisode: FilterEpisode;
  FilterLocation: FilterLocation;
  ID: ResolverTypeWrapper<Scalars['ID']['output']>;
  Info: ResolverTypeWrapper<Info>;
  Int: ResolverTypeWrapper<Scalars['Int']['output']>;
  Language: ResolverTypeWrapper<Language>;
  LanguageFilterInput: LanguageFilterInput;
  Location: ResolverTypeWrapper<Location>;
  Locations: ResolverTypeWrapper<Locations>;
  Query: ResolverTypeWrapper<{}>;
  State: ResolverTypeWrapper<State>;
  String: ResolverTypeWrapper<Scalars['String']['output']>;
  StringQueryOperatorInput: StringQueryOperatorInput;
  Subdivision: ResolverTypeWrapper<Subdivision>;
  Upload: ResolverTypeWrapper<Scalars['Upload']['output']>;
};

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = {
  Boolean: Scalars['Boolean']['output'];
  Character: Character;
  Characters: Characters;
  Continent: Continent;
  ContinentFilterInput: ContinentFilterInput;
  Country: Country;
  CountryFilterInput: CountryFilterInput;
  Episode: Episode;
  Episodes: Episodes;
  FilterCharacter: FilterCharacter;
  FilterEpisode: FilterEpisode;
  FilterLocation: FilterLocation;
  ID: Scalars['ID']['output'];
  Info: Info;
  Int: Scalars['Int']['output'];
  Language: Language;
  LanguageFilterInput: LanguageFilterInput;
  Location: Location;
  Locations: Locations;
  Query: {};
  State: State;
  String: Scalars['String']['output'];
  StringQueryOperatorInput: StringQueryOperatorInput;
  Subdivision: Subdivision;
  Upload: Scalars['Upload']['output'];
};

export type CacheControlDirectiveArgs = {
  maxAge?: Maybe<Scalars['Int']['input']>;
  scope?: Maybe<CacheControlScope>;
};

export type CacheControlDirectiveResolver<Result, Parent, ContextType = any, Args = CacheControlDirectiveArgs> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type CharacterResolvers<ContextType = any, ParentType extends ResolversParentTypes['Character'] = ResolversParentTypes['Character']> = {
  created?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  episode?: Resolver<Array<Maybe<ResolversTypes['Episode']>>, ParentType, ContextType>;
  gender?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>;
  image?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  location?: Resolver<Maybe<ResolversTypes['Location']>, ParentType, ContextType>;
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  origin?: Resolver<Maybe<ResolversTypes['Location']>, ParentType, ContextType>;
  species?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  status?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  type?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type CharactersResolvers<ContextType = any, ParentType extends ResolversParentTypes['Characters'] = ResolversParentTypes['Characters']> = {
  info?: Resolver<Maybe<ResolversTypes['Info']>, ParentType, ContextType>;
  results?: Resolver<Maybe<Array<Maybe<ResolversTypes['Character']>>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type ContinentResolvers<ContextType = any, ParentType extends ResolversParentTypes['Continent'] = ResolversParentTypes['Continent']> = {
  code?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  countries?: Resolver<Array<ResolversTypes['Country']>, ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type CountryResolvers<ContextType = any, ParentType extends ResolversParentTypes['Country'] = ResolversParentTypes['Country']> = {
  awsRegion?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  capital?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  code?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  continent?: Resolver<ResolversTypes['Continent'], ParentType, ContextType>;
  currencies?: Resolver<Array<ResolversTypes['String']>, ParentType, ContextType>;
  currency?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  emoji?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  emojiU?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  languages?: Resolver<Array<ResolversTypes['Language']>, ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType, Partial<CountryNameArgs>>;
  native?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  phone?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  phones?: Resolver<Array<ResolversTypes['String']>, ParentType, ContextType>;
  states?: Resolver<Array<ResolversTypes['State']>, ParentType, ContextType>;
  subdivisions?: Resolver<Array<ResolversTypes['Subdivision']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type EpisodeResolvers<ContextType = any, ParentType extends ResolversParentTypes['Episode'] = ResolversParentTypes['Episode']> = {
  air_date?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  characters?: Resolver<Array<Maybe<ResolversTypes['Character']>>, ParentType, ContextType>;
  created?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  episode?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>;
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type EpisodesResolvers<ContextType = any, ParentType extends ResolversParentTypes['Episodes'] = ResolversParentTypes['Episodes']> = {
  info?: Resolver<Maybe<ResolversTypes['Info']>, ParentType, ContextType>;
  results?: Resolver<Maybe<Array<Maybe<ResolversTypes['Episode']>>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type InfoResolvers<ContextType = any, ParentType extends ResolversParentTypes['Info'] = ResolversParentTypes['Info']> = {
  count?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  next?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  pages?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  prev?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type LanguageResolvers<ContextType = any, ParentType extends ResolversParentTypes['Language'] = ResolversParentTypes['Language']> = {
  code?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  native?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  rtl?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type LocationResolvers<ContextType = any, ParentType extends ResolversParentTypes['Location'] = ResolversParentTypes['Location']> = {
  created?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  dimension?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>;
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  residents?: Resolver<Array<Maybe<ResolversTypes['Character']>>, ParentType, ContextType>;
  type?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type LocationsResolvers<ContextType = any, ParentType extends ResolversParentTypes['Locations'] = ResolversParentTypes['Locations']> = {
  info?: Resolver<Maybe<ResolversTypes['Info']>, ParentType, ContextType>;
  results?: Resolver<Maybe<Array<Maybe<ResolversTypes['Location']>>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type QueryResolvers<ContextType = any, ParentType extends ResolversParentTypes['Query'] = ResolversParentTypes['Query']> = {
  character?: Resolver<Maybe<ResolversTypes['Character']>, ParentType, ContextType, RequireFields<QueryCharacterArgs, 'id'>>;
  characters?: Resolver<Maybe<ResolversTypes['Characters']>, ParentType, ContextType, Partial<QueryCharactersArgs>>;
  charactersByIds?: Resolver<Maybe<Array<Maybe<ResolversTypes['Character']>>>, ParentType, ContextType, RequireFields<QueryCharactersByIdsArgs, 'ids'>>;
  continent?: Resolver<Maybe<ResolversTypes['Continent']>, ParentType, ContextType, RequireFields<QueryContinentArgs, 'code'>>;
  continents?: Resolver<Array<ResolversTypes['Continent']>, ParentType, ContextType, RequireFields<QueryContinentsArgs, 'filter'>>;
  countries?: Resolver<Array<ResolversTypes['Country']>, ParentType, ContextType, RequireFields<QueryCountriesArgs, 'filter'>>;
  country?: Resolver<Maybe<ResolversTypes['Country']>, ParentType, ContextType, RequireFields<QueryCountryArgs, 'code'>>;
  episode?: Resolver<Maybe<ResolversTypes['Episode']>, ParentType, ContextType, RequireFields<QueryEpisodeArgs, 'id'>>;
  episodes?: Resolver<Maybe<ResolversTypes['Episodes']>, ParentType, ContextType, Partial<QueryEpisodesArgs>>;
  episodesByIds?: Resolver<Maybe<Array<Maybe<ResolversTypes['Episode']>>>, ParentType, ContextType, RequireFields<QueryEpisodesByIdsArgs, 'ids'>>;
  language?: Resolver<Maybe<ResolversTypes['Language']>, ParentType, ContextType, RequireFields<QueryLanguageArgs, 'code'>>;
  languages?: Resolver<Array<ResolversTypes['Language']>, ParentType, ContextType, RequireFields<QueryLanguagesArgs, 'filter'>>;
  location?: Resolver<Maybe<ResolversTypes['Location']>, ParentType, ContextType, RequireFields<QueryLocationArgs, 'id'>>;
  locations?: Resolver<Maybe<ResolversTypes['Locations']>, ParentType, ContextType, Partial<QueryLocationsArgs>>;
  locationsByIds?: Resolver<Maybe<Array<Maybe<ResolversTypes['Location']>>>, ParentType, ContextType, RequireFields<QueryLocationsByIdsArgs, 'ids'>>;
};

export type StateResolvers<ContextType = any, ParentType extends ResolversParentTypes['State'] = ResolversParentTypes['State']> = {
  code?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  country?: Resolver<ResolversTypes['Country'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type SubdivisionResolvers<ContextType = any, ParentType extends ResolversParentTypes['Subdivision'] = ResolversParentTypes['Subdivision']> = {
  code?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  emoji?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export interface UploadScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['Upload'], any> {
  name: 'Upload';
}

export type Resolvers<ContextType = any> = {
  Character?: CharacterResolvers<ContextType>;
  Characters?: CharactersResolvers<ContextType>;
  Continent?: ContinentResolvers<ContextType>;
  Country?: CountryResolvers<ContextType>;
  Episode?: EpisodeResolvers<ContextType>;
  Episodes?: EpisodesResolvers<ContextType>;
  Info?: InfoResolvers<ContextType>;
  Language?: LanguageResolvers<ContextType>;
  Location?: LocationResolvers<ContextType>;
  Locations?: LocationsResolvers<ContextType>;
  Query?: QueryResolvers<ContextType>;
  State?: StateResolvers<ContextType>;
  Subdivision?: SubdivisionResolvers<ContextType>;
  Upload?: GraphQLScalarType;
};

export type DirectiveResolvers<ContextType = any> = {
  cacheControl?: CacheControlDirectiveResolver<any, any, ContextType>;
};

export const namedOperations = {
  Query: {
    GetDemo: 'GetDemo'
  }
}
export type GetDemoQueryVariables = Exact<{ [key: string]: never; }>;


export type GetDemoQuery = { __typename: 'Query' };


export const GetDemoDocument = gql`
    query GetDemo {
  __typename
}
    `;

export function useGetDemoQuery(options: Omit<Urql.UseQueryArgs<never, GetDemoQueryVariables>, 'query'> = {}) {
  return Urql.useQuery<GetDemoQuery>({ query: GetDemoDocument, ...options });
};
export type WithTypename<T extends { __typename?: any }> = Partial<T> & { __typename: NonNullable<T['__typename']> };

export type GraphCacheKeysConfig = {
  Character?: (data: WithTypename<Character>) => null | string,
  Characters?: (data: WithTypename<Characters>) => null | string,
  Continent?: (data: WithTypename<Continent>) => null | string,
  Country?: (data: WithTypename<Country>) => null | string,
  Episode?: (data: WithTypename<Episode>) => null | string,
  Episodes?: (data: WithTypename<Episodes>) => null | string,
  Info?: (data: WithTypename<Info>) => null | string,
  Language?: (data: WithTypename<Language>) => null | string,
  Location?: (data: WithTypename<Location>) => null | string,
  Locations?: (data: WithTypename<Locations>) => null | string,
  State?: (data: WithTypename<State>) => null | string,
  Subdivision?: (data: WithTypename<Subdivision>) => null | string
}

export type GraphCacheResolvers = {
  Query?: {
    character?: GraphCacheResolver<WithTypename<Query>, QueryCharacterArgs, WithTypename<Character> | string>,
    characters?: GraphCacheResolver<WithTypename<Query>, QueryCharactersArgs, WithTypename<Characters> | string>,
    charactersByIds?: GraphCacheResolver<WithTypename<Query>, QueryCharactersByIdsArgs, Array<WithTypename<Character> | string>>,
    continent?: GraphCacheResolver<WithTypename<Query>, QueryContinentArgs, WithTypename<Continent> | string>,
    continents?: GraphCacheResolver<WithTypename<Query>, QueryContinentsArgs, Array<WithTypename<Continent> | string>>,
    countries?: GraphCacheResolver<WithTypename<Query>, QueryCountriesArgs, Array<WithTypename<Country> | string>>,
    country?: GraphCacheResolver<WithTypename<Query>, QueryCountryArgs, WithTypename<Country> | string>,
    episode?: GraphCacheResolver<WithTypename<Query>, QueryEpisodeArgs, WithTypename<Episode> | string>,
    episodes?: GraphCacheResolver<WithTypename<Query>, QueryEpisodesArgs, WithTypename<Episodes> | string>,
    episodesByIds?: GraphCacheResolver<WithTypename<Query>, QueryEpisodesByIdsArgs, Array<WithTypename<Episode> | string>>,
    language?: GraphCacheResolver<WithTypename<Query>, QueryLanguageArgs, WithTypename<Language> | string>,
    languages?: GraphCacheResolver<WithTypename<Query>, QueryLanguagesArgs, Array<WithTypename<Language> | string>>,
    location?: GraphCacheResolver<WithTypename<Query>, QueryLocationArgs, WithTypename<Location> | string>,
    locations?: GraphCacheResolver<WithTypename<Query>, QueryLocationsArgs, WithTypename<Locations> | string>,
    locationsByIds?: GraphCacheResolver<WithTypename<Query>, QueryLocationsByIdsArgs, Array<WithTypename<Location> | string>>
  },
  Character?: {
    created?: GraphCacheResolver<WithTypename<Character>, Record<string, never>, Scalars['String'] | string>,
    episode?: GraphCacheResolver<WithTypename<Character>, Record<string, never>, Array<WithTypename<Episode> | string>>,
    gender?: GraphCacheResolver<WithTypename<Character>, Record<string, never>, Scalars['String'] | string>,
    id?: GraphCacheResolver<WithTypename<Character>, Record<string, never>, Scalars['ID'] | string>,
    image?: GraphCacheResolver<WithTypename<Character>, Record<string, never>, Scalars['String'] | string>,
    location?: GraphCacheResolver<WithTypename<Character>, Record<string, never>, WithTypename<Location> | string>,
    name?: GraphCacheResolver<WithTypename<Character>, Record<string, never>, Scalars['String'] | string>,
    origin?: GraphCacheResolver<WithTypename<Character>, Record<string, never>, WithTypename<Location> | string>,
    species?: GraphCacheResolver<WithTypename<Character>, Record<string, never>, Scalars['String'] | string>,
    status?: GraphCacheResolver<WithTypename<Character>, Record<string, never>, Scalars['String'] | string>,
    type?: GraphCacheResolver<WithTypename<Character>, Record<string, never>, Scalars['String'] | string>
  },
  Characters?: {
    info?: GraphCacheResolver<WithTypename<Characters>, Record<string, never>, WithTypename<Info> | string>,
    results?: GraphCacheResolver<WithTypename<Characters>, Record<string, never>, Array<WithTypename<Character> | string>>
  },
  Continent?: {
    code?: GraphCacheResolver<WithTypename<Continent>, Record<string, never>, Scalars['ID'] | string>,
    countries?: GraphCacheResolver<WithTypename<Continent>, Record<string, never>, Array<WithTypename<Country> | string>>,
    name?: GraphCacheResolver<WithTypename<Continent>, Record<string, never>, Scalars['String'] | string>
  },
  Country?: {
    awsRegion?: GraphCacheResolver<WithTypename<Country>, Record<string, never>, Scalars['String'] | string>,
    capital?: GraphCacheResolver<WithTypename<Country>, Record<string, never>, Scalars['String'] | string>,
    code?: GraphCacheResolver<WithTypename<Country>, Record<string, never>, Scalars['ID'] | string>,
    continent?: GraphCacheResolver<WithTypename<Country>, Record<string, never>, WithTypename<Continent> | string>,
    currencies?: GraphCacheResolver<WithTypename<Country>, Record<string, never>, Array<Scalars['String'] | string>>,
    currency?: GraphCacheResolver<WithTypename<Country>, Record<string, never>, Scalars['String'] | string>,
    emoji?: GraphCacheResolver<WithTypename<Country>, Record<string, never>, Scalars['String'] | string>,
    emojiU?: GraphCacheResolver<WithTypename<Country>, Record<string, never>, Scalars['String'] | string>,
    languages?: GraphCacheResolver<WithTypename<Country>, Record<string, never>, Array<WithTypename<Language> | string>>,
    name?: GraphCacheResolver<WithTypename<Country>, CountryNameArgs, Scalars['String'] | string>,
    native?: GraphCacheResolver<WithTypename<Country>, Record<string, never>, Scalars['String'] | string>,
    phone?: GraphCacheResolver<WithTypename<Country>, Record<string, never>, Scalars['String'] | string>,
    phones?: GraphCacheResolver<WithTypename<Country>, Record<string, never>, Array<Scalars['String'] | string>>,
    states?: GraphCacheResolver<WithTypename<Country>, Record<string, never>, Array<WithTypename<State> | string>>,
    subdivisions?: GraphCacheResolver<WithTypename<Country>, Record<string, never>, Array<WithTypename<Subdivision> | string>>
  },
  Episode?: {
    air_date?: GraphCacheResolver<WithTypename<Episode>, Record<string, never>, Scalars['String'] | string>,
    characters?: GraphCacheResolver<WithTypename<Episode>, Record<string, never>, Array<WithTypename<Character> | string>>,
    created?: GraphCacheResolver<WithTypename<Episode>, Record<string, never>, Scalars['String'] | string>,
    episode?: GraphCacheResolver<WithTypename<Episode>, Record<string, never>, Scalars['String'] | string>,
    id?: GraphCacheResolver<WithTypename<Episode>, Record<string, never>, Scalars['ID'] | string>,
    name?: GraphCacheResolver<WithTypename<Episode>, Record<string, never>, Scalars['String'] | string>
  },
  Episodes?: {
    info?: GraphCacheResolver<WithTypename<Episodes>, Record<string, never>, WithTypename<Info> | string>,
    results?: GraphCacheResolver<WithTypename<Episodes>, Record<string, never>, Array<WithTypename<Episode> | string>>
  },
  Info?: {
    count?: GraphCacheResolver<WithTypename<Info>, Record<string, never>, Scalars['Int'] | string>,
    next?: GraphCacheResolver<WithTypename<Info>, Record<string, never>, Scalars['Int'] | string>,
    pages?: GraphCacheResolver<WithTypename<Info>, Record<string, never>, Scalars['Int'] | string>,
    prev?: GraphCacheResolver<WithTypename<Info>, Record<string, never>, Scalars['Int'] | string>
  },
  Language?: {
    code?: GraphCacheResolver<WithTypename<Language>, Record<string, never>, Scalars['ID'] | string>,
    name?: GraphCacheResolver<WithTypename<Language>, Record<string, never>, Scalars['String'] | string>,
    native?: GraphCacheResolver<WithTypename<Language>, Record<string, never>, Scalars['String'] | string>,
    rtl?: GraphCacheResolver<WithTypename<Language>, Record<string, never>, Scalars['Boolean'] | string>
  },
  Location?: {
    created?: GraphCacheResolver<WithTypename<Location>, Record<string, never>, Scalars['String'] | string>,
    dimension?: GraphCacheResolver<WithTypename<Location>, Record<string, never>, Scalars['String'] | string>,
    id?: GraphCacheResolver<WithTypename<Location>, Record<string, never>, Scalars['ID'] | string>,
    name?: GraphCacheResolver<WithTypename<Location>, Record<string, never>, Scalars['String'] | string>,
    residents?: GraphCacheResolver<WithTypename<Location>, Record<string, never>, Array<WithTypename<Character> | string>>,
    type?: GraphCacheResolver<WithTypename<Location>, Record<string, never>, Scalars['String'] | string>
  },
  Locations?: {
    info?: GraphCacheResolver<WithTypename<Locations>, Record<string, never>, WithTypename<Info> | string>,
    results?: GraphCacheResolver<WithTypename<Locations>, Record<string, never>, Array<WithTypename<Location> | string>>
  },
  State?: {
    code?: GraphCacheResolver<WithTypename<State>, Record<string, never>, Scalars['String'] | string>,
    country?: GraphCacheResolver<WithTypename<State>, Record<string, never>, WithTypename<Country> | string>,
    name?: GraphCacheResolver<WithTypename<State>, Record<string, never>, Scalars['String'] | string>
  },
  Subdivision?: {
    code?: GraphCacheResolver<WithTypename<Subdivision>, Record<string, never>, Scalars['ID'] | string>,
    emoji?: GraphCacheResolver<WithTypename<Subdivision>, Record<string, never>, Scalars['String'] | string>,
    name?: GraphCacheResolver<WithTypename<Subdivision>, Record<string, never>, Scalars['String'] | string>
  }
};

export type GraphCacheOptimisticUpdaters = {};

export type GraphCacheUpdaters = {
  Mutation?: {},
  Subscription?: {},
};

export type GraphCacheConfig = Parameters<typeof offlineExchange>[0] & {
  updates?: GraphCacheUpdaters,
  keys?: GraphCacheKeysConfig,
  optimistic?: GraphCacheOptimisticUpdaters,
  resolvers?: GraphCacheResolvers,
};

export const GetDemo = gql`
    query GetDemo {
  __typename
}
    `;