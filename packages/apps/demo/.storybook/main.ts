import type { StorybookConfig } from "@storybook-vue/nuxt";
import { mergeConfig } from 'vite'

import layers from '../.nuxt/vueql/server/layers'


const config: StorybookConfig = {
  stories: layers.map(layer => [
    `${layer}/**/*.mdx`,
    `${layer}/**/*.stories.@(js|jsx|ts|tsx)`
  ]).flat().concat([
    `${process.env.VUEQL_CORE_MODULE_PATH}/**/*.mdx`,
    `${process.env.VUEQL_CORE_MODULE_PATH}/**/*.stories.@(js|jsx|ts|tsx)`,
    `${process.env.VUEQL_MODULE_PATH}/**/*.mdx`,
    `${process.env.VUEQL_MODULE_PATH}/**/*.stories.@(js|jsx|ts|tsx)`
  ]),
  addons: [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-interactions",
  ],
  framework: {
    name: "@storybook-vue/nuxt",
    options: {},
  },
  docs: {
    autodocs: "tag",
  },
  managerHead: (head, { configType }) => {
    if (configType === 'PRODUCTION') {
      return (`
        ${head}
        <base href="/storybook/">
      `)
    }
  },
  async viteFinal(baseConfig, { configType }) {
    return mergeConfig(
      {
        ...(configType === 'PRODUCTION' ? { base: '/storybook/' } : {}),
      },
      baseConfig
    )
  }
}
export default config