// Define unique development and Vite HMR server ports per each app.
// NOTE: Don't forget to expose these ports in `docker-compose.yml`.
const NUXT_DEV_SERVER_PORT=3000
const NUXT_VITE_HMR_PORT=24678


// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },

  // @ts-ignore
  extends: [
    `${process.env.VUEQL_APP_PATH}/demo`
  ],

  // Modules:
  // * Use package name for 3rd-party library usage, or,
  // * Use file path for live module development (hot reloading, etc.)
  modules: [
    '@storybook-vue/nuxt-storybook',
    process.env.NODE_ENV === 'development' ? `${process.env.VUEQL_MODULE_PATH}/stackwrap/vueql-example-module/src/module` : '@stackwrap/vueql-example-module'
  ],

  devServer: {
    port: NUXT_DEV_SERVER_PORT
  },

  vite: {
    server: {
      hmr: {
        clientPort: NUXT_VITE_HMR_PORT,
        port: NUXT_VITE_HMR_PORT
      }
    }
  }
})
